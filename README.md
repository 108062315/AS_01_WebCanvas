# Software Studio 2021 Spring
## Assignment 01 Web Canvas - 108062315 莊子郁


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |



### How to use 

    Describe how to use your web and maybe insert images to help you explain.
    
    The capture below is my webcanvas. 
    We can control by mouse. 
    The default tool is pencil, and we can change tools and colors by clicking icons below.
    We and resize the brush by dragging the slider. 
    The "Choose File" button is for you to upload a image.
    The rightmost, Font-size and Font is used to adjust the text.
    
![](https://i.imgur.com/9yMvVzX.png)
    
### Function description

    Decribe your bouns function and how to use it.
    
    I would like to introduce from the leftmost.
    
    1. Pencil: 
        You can draw whatever you want. 
    2. Eraser: 
        You can fix the mistakes on the canvas. 
    3. Text:
        You can type inside.
    4. Shape tools:
        You can draw triangle, rectangle, and circle with them.
        And it's default is empty-inside.
    5. Filled(The star icon)
        This controls whether the shape you're going to draw is filled or not. 
    6. Undo(left arrow):
        Eraser is kind of hard to control, so the undo is always better choice.
    7.Redo(right arrow):
        If you are obsessed with undoing, redo may help you go back to the right place.
    8. Restart(green arrow)
        You can clear the canvas and it's cannot be redo.
    9. Download:
        You can download your masterpiece here!
    10. Color-Picker:
        You can seletct a color there.
    11.Brush-Size:
        You can adjust the size of brush by dragging.
    12.Upload:
        You can choose a image here, and it will upload to canvas.
    13.Font-Size:
        You can change the size of test, it has 12px, 24px, 36px to choose.
    14.Font:
        You can choose one from "sans-serif", "serif", and "Times New Roman".
    15. Reload remider:
        If you have some changes on canvas, and you click the refresh button(the website one, not my restart.), it will pop out and remind you that changes haven't saved.
        16. Canvas will adjust with the window after refresh.
        17. Cursor changes after click pencil, eraser, and shape tools. And it changes when you move to the tool set.
        
### Gitlab page link

   [https://108062315.gitlab.io/AS_01_WebCanvas](https://108062315.gitlab.io/AS_01_WebCanvas/)

### Others (Optional)

    除了pencil 和 eraser之外的icon都是用小畫家速成出來的。
    Cursor 用的是內建的圖案。
    

<style>
table th{
    width: 100%;
}
</style>