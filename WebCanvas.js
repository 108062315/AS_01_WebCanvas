const canvas = document.getElementById("canvas");

canvas.width = window.innerWidth -60;
canvas.height = 400;

let context = canvas.getContext("2d");
//context.fillStyle = "white";
//context.fillRect(0, 0, canvas.width, canvas.height);
let original_background_color = "white";

//draw elements

let draw_color = "black";
let draw_width = "20";
let is_drawing = false;

//undo && redo
let restore_array = [];
let index = 0;
let max_index = 0;
restore_array.push(context.getImageData(0, 0, canvas.width, canvas.height));

//tools

//penc
let penc = true;
//eraser
let clear = false;

//shapes
let shapeX, shapeY;
//rectangle
let rect = false;
let rectX, rectY, rectW, rectH;
//circle
let circ = false;
let cenX, cenY, radius;
//triangle
let tria = false;
//text
let type = false;
font = "14px Times New Roman";
//upload

let SEL = (sel) => document.querySelector(sel);
let ctx = SEL("canvas").getContext("2d");

//fill

let fillshape = false;

//ask before reload

window.onbeforeunload =function() {
    return "Reload site?";
}


//drawing
//for mobilephone 
//touch events
canvas.addEventListener("touchstart", start,false);
canvas.addEventListener("touchmove", draw,false);
canvas.addEventListener("touchend", stop,false);

//mouse events
canvas.addEventListener("mousedown", start,false);
canvas.addEventListener("mousemove", draw,false);
canvas.addEventListener("mouseup", stop,false);
canvas.addEventListener("mouseout", stop,false);

//upload
SEL("#upload").addEventListener("change", upload, false);


//drawing

function start(event) {
    is_drawing = true;
    //context.beginPath();
    if(is_drawing == true) {
        if(penc == true) {
            context.globalCompositeOperation = "source-over";
            context.beginPath();
            context.moveTo(event.clientX - canvas.offsetLeft, event.clientY - canvas.offsetTop);  
        }
        else if(clear == true) {
            context.globalCompositeOperation = "destination-out";
            context.beginPath();
            context.moveTo(event.clientX - canvas.offsetLeft, event.clientY - canvas.offsetTop); 
        }
        else if(rect == true) {
            context.globalCompositeOperation = "source-over";
            shapeX = event.clientX - canvas.offsetLeft;
            shapeY = event.clientY - canvas.offsetTop;
        }
        else if(circ == true) {
            context.globalCompositeOperation = "source-over";
            
            //context.moveTo(event.clientX - canvas.offsetLeft, event.clientY - canvas.offsetTop); 
            shapeX = event.clientX - canvas.offsetLeft;
            shapeY = event.clientY - canvas.offsetTop;            
        }
        else if(tria == true) {
            context.globalCompositeOperation = "source-over";
            
            shapeX = event.clientX - canvas.offsetLeft;
            shapeY = event.clientY - canvas.offsetTop; 
            
        }
        else if(type == true) {
            context.lineCap = "square";
            context.lineJoin = "square";
            shapeX = event.clientX - canvas.offsetLeft;
            shapeY = event.clientY - canvas.offsetTop;
            TextInput(shapeX, shapeY);
        }
        
    }
    //event.preventDefault();
}

function draw(event) {
    
    if(is_drawing == true) {
        if(penc == true) {
            context.lineTo(event.clientX - canvas.offsetLeft, event.clientY - canvas.offsetTop);
            context.strokeStyle = draw_color;
            context.lineWidth = draw_width;
            context.lineCap = "round";
            context.lineJoin = "round";
            context.stroke();//does not stop without up
        }
        else if(clear == true) {
            context.lineTo(event.clientX - canvas.offsetLeft, event.clientY - canvas.offsetTop);
            context.stroke();
        }
        else if(rect == true) {
            //context.lineWidth = draw_width;
            context.fillStyle = draw_color;
            context.strokeStyle = draw_color;
            rectX = Math.min(shapeX, event.clientX - canvas.offsetLeft);
            rectY = Math.min(shapeY, event.clientY - canvas.offsetTop);
            rectW = Math.abs(event.clientX - canvas.offsetLeft - shapeX);
            rectH = Math.abs(event.clientY - canvas.offsetTop - shapeY);
            context.putImageData(restore_array[index], 0, 0);
            context.strokeRect(rectX, rectY, rectW, rectH);
            if(fillshape) {
                context.fillRect(rectX, rectY, rectW, rectH);
            }
            //context.strokeRect(rectX, rectY, rectW, rectH);
        }
        else if(circ == true) {
            context.putImageData(restore_array[index], 0, 0);
            context.beginPath();
            context.strokeStyle = draw_color;
            context.fillStyle = draw_color;
            cenX = (event.clientX - canvas.offsetLeft + shapeX)/2;
            cenY = (event.clientY - canvas.offsetTop + shapeY)/2;
            let a = Math.abs(event.clientX - canvas.offsetLeft - shapeX)/2;
            let b =  Math.abs(event.clientY - canvas.offsetTop - shapeY)/2;
            radius = Math.sqrt(a*a+b*b);
            //console.log(a, b);
            
            context.arc(cenX, cenY, radius, 0, 360*Math.PI/180, false);
            context.stroke();
            if(fillshape){
                context.fill();
            }
        }
        else if(tria == true) {
            context.putImageData(restore_array[index], 0, 0);
            context.beginPath();
            context.strokeStyle = draw_color;
            context.fillStyle = draw_color;
            let triaX = Math.abs(event.clientX - canvas.offsetLeft + shapeX)/2;
            let triaY = Math.abs(event.clientY - canvas.offsetTop + shapeY)/2;
            //context.lineWidth = draw_width;
            context.moveTo(triaX, shapeY);
            context.lineTo(event.clientX - canvas.offsetLeft, event.clientY - canvas.offsetTop); 
            context.lineTo(shapeX, event.clientY - canvas.offsetTop);
            context.lineTo(triaX, shapeY);
            
            context.stroke();
            if(fillshape) {
                context.fill();
            }
        }

    }
   
    //event.preventDefault();
}

function stop(event) {

    if(is_drawing == true) {
        if(penc == true) {
            context.stroke();
            context.closePath();
            is_drawing = false;
        }
        else if(clear == true) {
            context.stroke();
            context.closePath();
            is_drawing = false;
        }
        else if(rect == true) {
            is_drawing = false;
        }
        else if(circ == true) {
            //context.stroke();
            context.closePath();
            is_drawing = false;
        }
        else if(tria == true) {
            
            context.closePath();
            
            is_drawing = false;
        }
        else if(type == true) {
            is_drawing = false;
        }
    }
    //event.preventDefault();

    //store what's happening for undo
    if(event.type != "mouseout") {
        while(max_index > index) {
            max_index -= 1;
            restore_array.pop();
        }
        restore_array.push(context.getImageData(0, 0, canvas.width, canvas.height));
        index += 1;
        max_index = index;
    }
    //console.log(restore_array);
    //to see what's happening in console
}

//pencil

function pencil() {
    document.body.style.cursor = "grabbing";
    penc = true;
    clear = false;
    rect = false;
    circ = false;
    tria = false;
    type = false;
}

//eraser

function eraser() {
    document.body.style.cursor = "grab";
    penc = false;
    clear = true;
    rect = false;
    circ = false;
    tria = false;
    type = false;
}

//text

function text() {
    document.body.style.cursor = "text";
    penc = false;
    clear = false;
    rect = false;
    circ = false;
    tria = false;
    type = true;
}

//draw triangle

function triangle() {
    document.body.style.cursor = "alias";
    penc = false;
    clear = false;
    rect = false;
    circ = false;
    tria = true;
    type = false;
}

//draw rectangle

function rectangle() {
    document.body.style.cursor = "copy";
    penc = false;
    clear = false;
    rect = true;
    circ = false;
    tria = false;
    type = false;
}

//draw circle

function circle() {
    document.body.style.cursor = "help";
    penc = false;
    clear = false;
    rect = false;
    circ = true;
    tria = false;
    type = false;
}

//undo

function undo() {
    if(index >= 0) {
        index -= 1;
        context.putImageData(restore_array[index], 0, 0);
    }
    /*
    if(index <= 0) {
        restart();//nothing inside
    }
    else {
        index -= 1;
        //restore_array.pop();
        context.putImageData(restore_array[index], 0, 0);
    }*/
}

//redo

function redo() {
    if(index < max_index) {
        index += 1;
        context.putImageData(restore_array[index], 0, 0);
    }

}
//restart to draw

function restart() {
    context.fillStyle = original_background_color;
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.fillRect(0, 0, canvas.width, canvas.height);

    //restart the array
    restore_array = [];
    index = -1;
}

//download

function download() {
    let imgUrl = canvas.toDataURL("image/png");
    let saveA = document.createElement("a");
    document.body.appendChild(saveA);
    saveA.href = imgUrl;
    saveA.download = "zspic" + (new Date).getTime();
    saveA.target = "_blank";
    saveA.click();
}

//upload
/*
input.addEventListener("change", function(event){
    const file = this.files;
    const url = URL.createObjectURL(file)
    const img = new Image;
    img.alt = file.name;
    img.addEventListener('load', file, false);
    img.src = url;
    canvas.drawImage(file, 0, 0);
}, false);
*/

function upload() {
    if(!this.files || !this.files[0]) return;
    context.clearRect(0, 0, canvas.width, canvas.height);
    let readFile = new FileReader();
    readFile.addEventListener("load", (evt) => {
        let img = new Image();
        img.addEventListener("load", () => {
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
            ctx.drawImage(img, 0, 0);
        });
        img.src = evt.target.result;
    });
    readFile.readAsDataURL(this.files[0]);
}

//text

let textsize, textfont;

function TextInput(x, y) {
    textsize = document.getElementById("typeSize");
    textfont = document.getElementById("typeFont");

    console.log(textsize.value, textfont.value);

    let input = document.createElement("input");
    input.type = "text";
    input.style.position = "fixed";
    input.style.left = (x - 4) + "px";
    input.style.top = (y - 4) + "px";
    input.onkeydown = handleEnter;
    document.body.appendChild(input);
    input.focus();
    hasInput = true;
}

function handleEnter(e) {
    let keyCode = e.keyCode;
    if (keyCode === 13) {
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        type = false;
    }
}

function drawText(txt, x, y) {
    let fontinfo = textsize.value + textfont.value;
    context.textBaseline = "top";
    context.textAlign = "left";
    context.font = fontinfo;
    context.fillText(txt, x, y);
}

function filled() {
    if(fillshape == false){
        fillshape = true;
    }
    else {
        fillshape = false;
    }
}
